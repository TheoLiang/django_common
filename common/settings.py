# -*- coding: utf-8 -*-
# @Author: theo-l
# @Date:   2017-09-08 10:22:29
# @Last Modified by:   theo-l
# @Last Modified time: 2017-09-08 11:01:26

# API response status code & message pair
RESPONSE_CODE_NAME = 'code'
RESPONSE_MSG_NAME = 'msg'


# common model fields, which used in the base resource class
MODEL_COMMON_FIELDS = ['created_at', 'enabled', 'updated_at']
