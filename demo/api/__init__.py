# -*- coding: utf-8 -*-
# @Author: theo-l
# @Date:   2017-09-08 12:02:25
# @Last Modified by:   theo-l
# @Last Modified time: 2017-09-08 12:05:13

from .member_api import MemberResource
from .demo_api import DemoResource
