# -*- coding: utf-8 -*-
# @Author: theo-l
# @Date:   2017-09-08 12:02:37
# @Last Modified by:   theo-l
# @Last Modified time: 2017-09-08 12:04:24

from common.base_resources import BaseModelResource
from demo.models import Member


class MemberResource(BaseModelResource):

    class Meta:
        object_class = Member
