# -*- coding: utf-8 -*-
# @Author: theo-l
# @Date:   2017-09-08 12:02:44
# @Last Modified by:   theo-l
# @Last Modified time: 2017-09-08 12:03:34
from common.base_resources import BaseModelResource

from demo.models import Demo, Member


class DemoResource(BaseModelResource):

    class Meta:
        object_class = Demo
