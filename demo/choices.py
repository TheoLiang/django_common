# -*- coding: utf-8 -*-
# @Author: theo-l
# @Date:   2017-09-08 11:56:36
# @Last Modified by:   theo-l
# @Last Modified time: 2017-09-08 11:57:44

from common.base_choices import ChoiceItem, BaseChoice


class UserType(BaseChoice):
    EXTERNAL = ChoiceItem(1, u'External')
    INTERNAL = ChoiceItem(2, u'Internal')
