# -*- coding: utf-8 -*-
# @Author: theo-l
# @Date:   2017-09-08 09:44:51
# @Last Modified by:   theo-l
# @Last Modified time: 2017-09-08 11:58:57
from django.db import models
from django.contrib.auth.models import AbstractBaseUser

from common.base_models import BaseModel

from .choices import UserType


class Member(AbstractBaseUser, BaseModel):
    phone = models.CharField(max_length=20, verbose_name=u'Phone Number')
    usertype = models.IntegerField(choices=UserType.choices, default=UserType.EXTERNAL, verbose_name=u'User Type')

    class Meta:
        verbose_name = verbose_name_plural = u'User'

    def __str__(self):
        return self.phone


class Demo(BaseModel):
    member = models.ForeignKey(Member, verbose_name=u'Resource Owner')
    title = models.CharField(max_length=50, verbose_name=u'Title')

    class Meta:
        verbose_name = verbose_name_plural = u'Demo'

    def __str__(self):
        return self.title
