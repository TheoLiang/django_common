# Django项目通用模块common #

**版本 v1.0**

### 1. Repo简要说明 ###

1. 集中维护管理和使用Django项目中的通用的一些实用模块，包括:

	- 通用的API消息代码 --用来统一在我们的项目中的一些常用的异常消息状态定义--
	- 通用的tastypie的model资源基类
	- 通用的Django的model基类
	- 通用的Fabric设置的基类
	- 通用的Django的model字段的Choice设置基类
	- 受tastypie的启发的一个结合Django的ListView和ModelFormView的超级视图类，主要用来控制后台管理界面
	- 通用的微信(Wechat)相关类
	- ...待续




### 2. 使用说明 ###

1. 当前该repo为一个Django的demo，其中的common APP就是我们的通用模块. 直接将common目录复制到我们的Django项目中
2. 依赖:
	- 见 requirements.txt
	
### 3. 通用使用模块使用样例 ###

1. Django模型字段中的choice基类使用说明
	
		class ColorChoice(BaseChoice):
            RED = ChoiceItem(1, 'Red')
            GREEN = ChoiceItem(2, 'Green')

        # in model definition
        class DemoModel(models.Model):
            color = models.IntegerField(choices=ColorChoice.choices, default=ColorChoice.RED)
			
2. API 常见的消息代码模块:
	
	参考 messagecode.py

3. Fabric 设置的使用说明
		
		from fabric.api import (env, task, hosts,roles)
		from fabrics import HostConfig

		# Remote host general configurations
		HOST_CONFIG = [
		    # host_string, password, host_name, role_name
    		('vagrant@192.168.33.10:22', 'vagrant', 'dev', 'dev')
		]



		host_config = HostConfig(host_config=HOST_CONFIG)
		host_config.setup_fabric_env(env)
		
		@task
		@hosts(host_config.DEV_HOSTS) # DEV_HOSTS 对应HOST_CONFIG 中元组中的hostname, 可以用来管理相同host_name 的所有主机
		def setup_dev():
    		setUp()
			
		or
		
		@task
		@roles('dev') # DEV_HOSTS 对应HOST_CONFIG 中元组中的 role_name, 可以用来管理相同role 的所有主机
		def setup_dev():
    		setUp()

4. 超级类视图XView的使用说明，简化admin视图中的一些通用的重复逻辑，如关键字检索，上下文环境，列表/详细页面之间的操作跳转等

		class ArticleView(XView):
    		app_label = 'backend' #用来指定当前视图所在的app，主要用在 revserse 来反向解析url
    		model = Article
    		paginate_by = 10 #指定列表页面中每页显示的数据
    		context_object_list_name = 'article_list' #列表页面中资源列表对象的名称
    		context_object_name = 'article_obj' # 详细页面中资源对象的名称
    		list_template_name = 'backend/article/list.html' #列表视图的模板
    		detail_template_name = 'backend/article/detail.html' #详细视图的模板
    		search_fields = ['title', 'author'] #列表视图中的搜索表单关键字搜索字段
    		form_class = ArticleModelForm #详细视图中的表单对象

			# 用来在资源的列表视图中添加上下文环境变量
    		def get_list_context_data(self, **kwargs):
        		data = super(ArticleView, self).get_list_context_data(**kwargs)
        		if 'paginator' in data and data['paginator']:
            		data["total_page_count"] = data['paginator'].num_pages
            		data["current_page"] = data['page_obj'].number
        		else:

            		data["total_page_count"] = 1
            		data["current_page"] = 1
        		return data

			#用来在资源的详细视图中添加上下文环境变量
    		def get_detail_context_data(self, **kwargs):
        		data = super(ArticleView, self).get_detail_context_data(**kwargs)
        		data["post_displaymode_choices"] = PostDisplayMode.CHOICES
        		return data
		

5. tastypie model资源基类使用说明

		

6. Django model基类使用说明

7. Wechat相关类使用说明






	
